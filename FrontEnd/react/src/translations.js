module.exports = {
  en: {
    locale: 'en-US',
    messages: {
      title: 'Drink & Smoke',
      switchToEn: 'Switch to English',
      switchToRu: 'Switch to Russian',
      loadingArticles: 'Loading Articles',
      home: 'Home',
      createArticle: 'Create Article',
      createUser: 'Create User',
      youLoggedAs: 'You are logged as {name}',
      loginTitle: 'Login',
      login: 'Login',
      logout: 'Logout',
      createUserTitle: 'Create User',
      selectRole: 'Select Role',
      create: 'Create',
      newArticle: 'New Article'
    }
  },
  ru: {
    locale: 'ru-RU',
    messages: {
      title: 'Бухай & Кури',
      switchToEn: 'На Английский',
      switchToRu: 'На Русский',
      loadingArticles: 'Загрузка статей',
      home: 'Домашняя',
      createArticle: 'Создать статью',
      createUser: 'Создать пользователя',
      youLoggedAs: 'Вы вошли под пользователем {name}',
      loginTitle: 'Авторизация',
      login: 'Авторизоваться',
      logout: 'Выйти из системы',
      createUserTitle: 'Создание Пользователя',
      selectRole: 'Выберите роль',
      create: 'Создать',
      newArticle: 'Новая статья'
    }
  }
}