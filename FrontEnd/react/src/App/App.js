import React from 'react'
import { Main } from '../_components'

export class App extends React.Component {
  render() {
    return (
      <div className='grid-container'>
        <Main />
      </div>
    )
  }
}