import { userConstants } from '../_constants'
import { userService } from '../_services'
import { history } from '../_helpers'

export const userActions = {
  login,
  logout,
  create
}

function login(creds) {
  return dispatch => {
    dispatch(request(creds.username));

    userService.login(creds)
      .then(
        user => {
          dispatch(success(user));
          history.push('/');
        },
        error => {
          dispatch(failure(error.toString()));
        }
      );
  };

  function request(user) {
    return {
      type: userConstants.LOGIN_REQUEST,
      user
    }
  }

  function success(user) {
    return {
      type: userConstants.LOGIN_SUCCESS,
      user
    }
  }

  function failure(error) {
    return {
      type: userConstants.LOGIN_FAILURE,
      error
    }
  }
}

function logout() {
  userService.logout();

  return {
    type: userConstants.LOGOUT
  }
}

function create(user) {
  return dispatch => {
    dispatch(request(user.username))

    userService.create(user)
      .then(
        user => {
          dispatch(success(user));
          history.push('/');
        },
        error => {
          dispatch(failure(error.toString()));
        }
      );
  }

  function request(user) {
    return {
      type: userConstants.CREATE_REQUEST,
      user
    }
  }

  function success(user) {
    return {
      type: userConstants.CREATE_SUCCESS,
      user
    }
  }

  function failure(error) {
    return {
      type: userConstants.CREATE_FAILURE,
      error
    }
  }
}