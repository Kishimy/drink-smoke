import { articleConstants } from '../_constants'
import { articleService } from '../_services'
import { history } from '../_helpers'

export const articleActions = {
  create,
  getAll,
  getById
}

function create(article) {
  return dispatch => {
    dispatch(request(article.title));

    articleService.create(article)
      .then(
        article => {
          dispatch(success(article));
          history.push('/');
        },
        error => {
          dispatch(failure(error));
        }
    )
  }

  function request(article) {
    return {
      type: articleConstants.CREATE_REQUEST,
      article
    }
  }

  function success(article) {
    return {
      type: articleConstants.CREATE_SUCCESS,
      article
    }
  }

  function failure(error) {
    return {
      type: articleConstants.CREATE_FAILURE,
      error
    }
  }
}

function getAll() {
  return dispatch => {
    dispatch(request());

    articleService.getAll()
      .then(
        articles => {
          dispatch(success(articles));
        },
        error => {
          dispatch(failure(error));
        }
      )
  }

  function request() {
    return {
      type: articleConstants.GETALL_REQUEST
    }
  }

  function success(articles) {
    return {
      type: articleConstants.GETALL_SUCCESS,
      articles
    }
  }

  function failure(error) {
    return {
      type: articleConstants.GETALL_FAILURE,
      error
    }
  }
}

function getById(id) {
  return dispatch => {
    dispatch(request())

    articleService.getById(id)
      .then(
        article => {
          dispatch(success(article));
        },
        error => {
          dispatch(failure(error));
        }
      )
  }

  function request() {
    return {
      type: articleConstants.GET_REQUEST
    }
  }

  function success(article) {
    return {
      type: articleConstants.GET_SUCCESS,
      article
    }
  }

  function failure(error) {
    return {
      type: articleConstants.GET_FAILURE,
      error
    }
  }
}