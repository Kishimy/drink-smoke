import config from 'config';

export const userService = {
  login,
  logout,
  create
}

function login(creds) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(creds)
  }

  return fetch(`${config.apiUrl}/auth/login`, requestOptions)
    .then(res => {
      return res.json()
    })
    .then(json => {
      if (json.success) {
        localStorage.setItem('user', JSON.stringify(json.user))
      }

      return json.user;
    })
}

function logout() {
  localStorage.removeItem('user')
}

function create(user) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(user)
  }

  return fetch(`${config.apiUrl}/auth/create`, requestOptions)
  .then(res => {
    return res.json().then(json => {
      if (json.success) {
        console.log('User succesfully created');
        return
      }

      const error = json.message;
      return Promise.reject(error);
    })
  })
}