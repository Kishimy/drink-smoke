import config from 'config';

export const articleService = {
  create,
  getAll,
  getById
}

function create(article) {
  const requestOptions = {
    method: 'POST',
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify(article)
  }

  return fetch(`${config.apiUrl}/articles/create`, requestOptions)
    .then(res => {
      return res.json().then(json => {
        if (json.success) {
          console.log('Article succesfully created');
          return
        }

        const error = json.message;
        return Promise.reject(error);
      })
    })
}

function getAll() {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  }

  return fetch(`${config.apiUrl}/articles/`, requestOptions)
    .then(res => {
      return res.json().then(json => {
        if (json.success) {
          console.log('Article succesfully recived');
          return json.articles
        }

        const error = json.message;
        return Promise.reject(error);
      })
    })
}

function getById(id) {
  const requestOptions = {
    method: 'GET',
    headers: { 'Content-Type': 'application/json' }
  }

  return fetch(`${config.apiUrl}/articles/${id}`, requestOptions)
    .then(res => {
      return res.json().then(json => {
        if (json.success) {
          console.log('Article succesfully recived');
          return json.article
        }

        const error = json.message;
        return Promise.reject(error);
      })
    })
}
