import { articleConstants } from '../_constants';

export function articles(state = {}, action) {
  switch (action.type) {
    case articleConstants.CREATE_REQUEST:
      return { 
        creating: true
      };
    case articleConstants.CREATE_SUCCESS:
      return {}
    case articleConstants.CREATE_FAILURE:
      return {}

    case articleConstants.GETALL_REQUEST:
      return { 
        loading: true
      };
    case articleConstants.GETALL_SUCCESS:
      return {
        items: action.articles
      };
    case articleConstants.GETALL_FAILURE:
      return {
        error: action.error
      }

    case articleConstants.GET_REQUEST:
      return { 
        loading: true
      };
    case articleConstants.GET_SUCCESS:
      return {
        article: action.article
      };
    case articleConstants.GET_FAILURE:
      return {
        error: action.error
      }

    default:
      return state
  }
}