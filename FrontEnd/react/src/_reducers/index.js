import { combineReducers } from 'redux'
import { IntlReducer as Intl } from 'react-redux-multilingual'

import { auth } from './auth.reducer'
import { articles } from './article.reducer'
import { users } from './users.reducer'

const rootReducer = combineReducers(
  Object.assign(
    {
      auth,
      articles,
      users
    },
    { Intl }
  )
)

export default rootReducer;