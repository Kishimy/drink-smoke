import React from 'react'
import { connect } from 'react-redux'
import ReactQuill from 'react-quill'
import { withTranslate } from 'react-redux-multilingual'
import 'react-quill/dist/quill.snow.css'

import { userActions } from '../_actions'
import { articleActions } from '../_actions'
import { history } from '../_helpers'

class ArticleCreatePage extends React.Component {
  constructor(props) {
    super(props)

    const { loggedIn } = this.props;

    if (!loggedIn) {
      this.props.dispatch(userActions.logout());
      history.push('/');
    }

    this.state = {
      title: '',
      content: ''
    }
  }

  handleArticleCreate = () => {
    this.props.dispatch(articleActions.create({
      title: this.state.title,
      content: this.state.content,
      token: JSON.parse(localStorage.getItem('user')).token
    }));
  }

  handleContentChange = value => {
    this.setState({ content: value })
  }

  handleTitleChange = e => {
    this.setState({ title: e.target.value })
  }

  render() {
    const { translate } = this.props
    const modules = {
      toolbar: [
        [{ 'header': [1, 2, false] }],
        ['bold', 'italic', 'underline','strike', 'blockquote'],
        [{'list': 'ordered'}, {'list': 'bullet'}, {'indent': '-1'}, {'indent': '+1'}],
        ['link', 'image'],
        ['clean']
      ],
    }
  
    const formats = [
      'header',
      'bold', 'italic', 'underline', 'strike', 'blockquote',
      'list', 'bullet', 'indent',
      'link', 'image'
    ]

    return (
      <div>
        <h1>
          {translate('newArticle')}
        </h1>
        <input type='text' 
          placeholder='Article Name'
          onChange={this.handleTitleChange} />
        <ReactQuill value={this.state.content}
          onChange={this.handleContentChange}
          modules={modules}
          formats={formats} />
        <button type='button' 
          className='button margin-top-1' 
          onClick={this.handleArticleCreate}>
          {translate('create')}
        </button>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { loggedIn } = state.auth;

  return {
    loggedIn
  };
}

const connectedArticleCreatePage = connect(mapStateToProps)(withTranslate(ArticleCreatePage));
export { connectedArticleCreatePage as ArticleCreatePage };