import React from 'react'
import { render } from 'react-dom'
import { IntlProvider } from 'react-redux-multilingual'
import { Provider } from 'react-redux';

import { store } from './_helpers';
import { App } from './App'
import translations from './translations'

import '../public/assets/app.scss';

render(
  <Provider store={store}>
    <IntlProvider translations={translations}>
      <App />
    </IntlProvider>
  </Provider>,
  document.getElementById('app')
);