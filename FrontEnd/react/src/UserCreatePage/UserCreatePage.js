import React from 'react'
import { connect } from 'react-redux'
import { withTranslate } from 'react-redux-multilingual'

import { userActions } from '../_actions'

class UserCreatePage extends React.Component {
  constructor(props) {
    super(props)

    const { loggedIn } = this.props;

    if (!loggedIn) {
      this.props.dispatch(userActions.logout());
      history.push('/');
    }

    this.state = {
      username: '',
      password: '',
      role: ''
    }
  }

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({ submitted: true });
    const { username, password, role } = this.state;
    const { dispatch } = this.props;

    if (username && password) {
      dispatch(userActions.create({
        username: username,
        password: password,
        role: role,
        token: JSON.parse(localStorage.getItem('user')).token
      }))
    }
  }

  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }

  render() {
    const { translate } = this.props

    return (
      <form onSubmit={this.handleSubmit}>
        <h1>
          {translate('createUserTitle')}
        </h1>
        <input type='text' 
          placeholder='Username'
          id='username'
          onChange={this.handleChange} />
        <input type='text' 
          placeholder='Password'
          id='password'
          onChange={this.handleChange} />
        {translate('selectRole')}
        <select
          id='role'
          onChange={this.handleChange}>
          <option>Admin</option>
          <option>User</option>
          <option>Guest</option>
        </select>
        <button type='submit' 
          className='button margin-top-1'>
          {translate('createUser')}
        </button>
      </form>
    )
  }
}

function mapStateToProps(state) {
  const { loggedIn } = state.auth;

  return {
    loggedIn
  };
}

const connectedUserCreatePage = connect(mapStateToProps)(withTranslate(UserCreatePage));
export { connectedUserCreatePage as UserCreatePage };