import React from 'react'
import { connect } from 'react-redux'
import { withTranslate } from 'react-redux-multilingual'

import { userActions } from '../_actions'

class Login extends React.Component {
  constructor(props) {
    super(props);

    // reset login status
    this.props.dispatch(userActions.logout());

    this.state = {
      username: '',
      password: '',
      submitted: false
    }
  }

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ submitted: true });
    const { username, password } = this.state;
    const { dispatch } = this.props;

    if (username && password) {
      dispatch(userActions.login({
        username: username,
        password: password
      }))
    }
  }

  handleChange = event => {
    this.setState({ [event.target.id]: event.target.value });
  }
  
  render() {
    const { translate } = this.props

    return(
      <div>
        <h1>{translate('loginTitle')}</h1>
        <form onSubmit={this.handleSubmit} >
          <input 
            type='text'
            id='username'
            placeholder='Username'
            onChange={this.handleChange}
          />
          <input 
            type='password'
            id='password'
            placeholder='Password'
            onChange={this.handleChange}
          />
          <button 
            type='submit' 
            className='expanded button'>
            {translate('login')}
          </button>
        </form>
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { loggedIn } = state.auth;

  return {
    loggedIn
  };
}

const connectedLoginPage = connect(mapStateToProps)(withTranslate(Login));
export { connectedLoginPage as Login };