import React from 'react'
import { connect } from 'react-redux'

import { articleActions } from '../_actions'

class ArticlePage extends React.Component {
  componentDidMount() {
    const id = this.props.match.params.id;
    this.props.dispatch(articleActions.getById(id));
  }

  render() {
    const { articles } = this.props

    return (
      <div className='article-page'>
        {articles.loading && <em>Loading articles...</em>}
        {articles.article &&
          <div className='article'>
            <h1>
              {articles.article.title}
            </h1>
            <div>
              Created at: {new Intl.DateTimeFormat('en-GB', {
                year: 'numeric', 
                month: 'long',
                day: '2-digit'})
                .format(new Date(articles.article.date))}
            </div>
            <div dangerouslySetInnerHTML={{__html: articles.article.content}}></div>
          </div>
        }
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { articles } = state

  return {
    articles
  };
}
  
const connectedArticlePage = connect(mapStateToProps)(ArticlePage);
export { connectedArticlePage as ArticlePage };