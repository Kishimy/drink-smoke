import React from 'react'
import { connect } from 'react-redux'
import { withTranslate } from 'react-redux-multilingual'

import { ArticleList } from '../_components/ArticleList'

class Home extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { translate } = this.props

    return (
      <div>
        <h1>
          {translate('title')}
        </h1>        
        <ArticleList />
      </div>
    )
  }
}

const connectedHomePage = connect()(withTranslate(Home));
export { connectedHomePage as Home };