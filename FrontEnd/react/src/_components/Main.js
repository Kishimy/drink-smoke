import React from 'react'
import { Router, Route } from 'react-router-dom';

import { history } from '../_helpers'

//components
import { UserMenu } from './UserMenu'
import { Nav } from './Nav'
import { LangSwitcher } from './LangSwitcher'

//pages
import { ArticleCreatePage } from '../ArticleCreatePage'
import { UserCreatePage } from '../UserCreatePage'
import { Home } from '../Home'
import { Login } from '../Login'
import { ArticlePage } from '../ArticlePage'

export class Main extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Router history={history}>
        <main>
          <div className='row align-justify align-middle'>
            <Nav />
            <LangSwitcher />
            <UserMenu />
          </div>
          <Route exact path='/' component={Home} />
          <Route path='/login' component={Login} />
          <Route path='/articleCreate' component={ArticleCreatePage} />
          <Route path='/userCreate' component={UserCreatePage} />
          <Route path='/article/:id' component={ArticlePage} />
        </main>
      </Router>
    )
  }
}