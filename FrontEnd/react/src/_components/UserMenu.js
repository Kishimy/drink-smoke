import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom'
import { withTranslate } from 'react-redux-multilingual'

import { history } from '../_helpers';
import { userActions } from '../_actions';

class UserMenu extends React.Component {
  constructor(props) {
    super(props);
  }

  handleLogout = event => {
    event.preventDefault();
    const { dispatch } = this.props;

    dispatch(userActions.logout());
    history.push('/');
  }

  render() {
    const { user, loggedIn, translate } = this.props;
    let content;

    if (loggedIn) {
      content = (
        <div>
          {translate('youLoggedAs', {name: user.name})}
          <div className='user-info__logout'>
            <a href='#' onClick={this.handleLogout}>{translate('logout')}</a>
          </div>
        </div>
      )
    } else {
      content = (
        <Link to='/login'>{translate('login')}</Link>
      )
    }

    return (
      <div className='user-info'>
        {content}
      </div>
    )
  }
}

function mapStateToProps(state) {
  const { user, loggedIn } = state.auth;

  return {
    user,
    loggedIn
  };
}

const connectedUserMenu = connect(mapStateToProps)(withTranslate(UserMenu));
export { connectedUserMenu as UserMenu };