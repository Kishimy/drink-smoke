import React from 'react'
import { withTranslate, IntlActions } from 'react-redux-multilingual'
import { connect } from 'react-redux'

class LangSwitcher extends React.Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { translate, dispatch } = this.props

    return (
      <div className='lang-switcher'>
        <button
          onClick={() => {
            dispatch(IntlActions.setLocale('en'))
          }}>
          {translate('switchToEn')}
        </button>
        <button
          onClick={() => {
            dispatch(IntlActions.setLocale('ru'))
          }}>
          {translate('switchToRu')}
        </button>
      </div>
    )
  }
}

const connectedLangSwitcher = connect()(withTranslate(LangSwitcher));
export { connectedLangSwitcher as LangSwitcher };