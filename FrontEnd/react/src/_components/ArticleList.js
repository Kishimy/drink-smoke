import React from 'react'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { withTranslate } from 'react-redux-multilingual'

import { articleActions } from '../_actions'

class ArticleList extends React.Component {
  componentDidMount() {
    this.props.dispatch(articleActions.getAll())
  }

  render() {
    const { articles, translate } = this.props

    return (
      <div className='article-list'>
        {articles.loading && <em>{translate('loadingArticles')}...</em>}
        {articles.items &&
          <ul>
            {articles.items.map(article => 
              <li key={article._id}>
                <Link to={`/article/${article._id}`}>{article.title}</Link>
              </li>
            )}
          </ul>
        }
      </div>
    )
  }
}

function mapToStateProps(state) {
  const { articles } = state

  return {
    articles
  }
}

const connectedArticleList = connect(mapToStateProps)(withTranslate(ArticleList));
export { connectedArticleList as ArticleList }