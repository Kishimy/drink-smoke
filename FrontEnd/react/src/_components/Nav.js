import React from 'react'
import { Link } from 'react-router-dom'
import { withTranslate } from 'react-redux-multilingual'
import { connect } from 'react-redux'

class Nav extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { loggedIn, translate } = this.props;

    return (
      <nav>
        <ul className='menu'>
          <li>
            <Link to='/'>{translate('home')}</Link>
          </li>
          {loggedIn ? (
            <li>
              <Link to='/userCreate'>{translate('createUser')}</Link>
            </li>
          ) : ''}
          {loggedIn ? (
            <li>
              <Link to='/articleCreate'>{translate('createArticle')}</Link>
            </li>
          ) : ''}
        </ul>
      </nav>
    )
  }
}
function mapStateToProps(state) {
  const { loggedIn } = state.auth;
  return {
    loggedIn
  };
}

const connectedNav = connect(mapStateToProps)(withTranslate(Nav));
export { connectedNav as Nav };