import Vue from 'vue'
import Vuex from 'vuex'
import login from './modules/login'
import signUp from './modules/signUp'
import product from './modules/product'
import alertMsg from './modules/alertMsg'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    login,
    signUp,
    product,
    alertMsg
  }
})
