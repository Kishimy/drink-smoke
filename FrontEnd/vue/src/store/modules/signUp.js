import auth from '../../api/auth';

const state = {
  userName: '',
  alert: ''
}

const getters = {
  alert: state => {
    return state.alert
  }
}

const mutations = {
  SIGNUP_SUCCESS: (state) => {
    state.alert = 'user succesfully created'
  },

  SIGNUP_FAILURE: (state) => {
    state.alert = 'failed to create user'
  }
}

const actions = {
  signUp({commit}, opt) {
    let data = {
      name: opt.user.email,
      password: opt.user.password,
      token: localStorage.token
    }

    auth.auth(data, (res) => {
      console.log('success')

      if (res.success) {
        commit('SIGNUP_SUCCESS')
        opt.callback()
        return
      }
    })
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
