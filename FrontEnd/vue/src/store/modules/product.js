const state = {
  productTitle: 'List of products',
  pruductList: [
    'Product one',
    'Product two',
    'Product three'
  ]
}

const getters = {
  countLinks: state => {
    return state.pruductList.length
  }
}

const mutations = {
  ADD_PRODUCT: (state, product) => {
    state.pruductList.push(product)
  },

  REMOVE_PRODUCT: (state, product) => {
    state.pruductList.splice(product, 1)
  },

  REMOVE_ALL: (state) => {
    state.pruductList = []
  }
}

const actions = {
  removeProduct: ({commit}, product) => {
    commit("REMOVE_PRODUCT", product)
  },

  removeAll({commit}) {
    return new Promise((resolve, reject) => {
      commit('REMOVE_ALL')
      resolve()
    })
  },
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
