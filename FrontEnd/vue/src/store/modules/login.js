import auth from '../../api/auth';

const state = {
  isLoggedIn: false
}

const getters = {
  isLoggedIn: state => {
    return state.isLoggedIn
  }
}

const mutations = {
  LOGIN: (state) => {
    state.pending = true
  },

  LOGIN_SUCCESS: (state) => {
    state.isLoggedIn = true
    state.pending = false
  },

  LOGOUT: (state) => {
    state.isLoggedIn = false
  }
}

const actions = {
  login({commit}, creds) {
    commit('LOGIN')

    auth.login(creds, (res) => {
      console.log(res.message);

      if (res.success) {
        localStorage.setItem('token', res.token)
        commit('LOGIN_SUCCESS')
      }
    })
  },

  logout({commit}) {
    localStorage.removeItem('token')
    commit('LOGOUT')
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
