const state = {
  alertMsg: ''
}

const getters = {
  alertMsg: state => {
    return state.alertMsg
  }
}

const mutations = {
  ALERT_MSG: (state, msg) => {
    state.alertMsg = msg
  }
}

export default {
  namespaced: true,
  state,
  getters,
  mutations
}
