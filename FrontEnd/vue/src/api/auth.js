import Vue from 'vue'
import VueAxios from 'vue-axios';
import Axios from 'axios';

Vue.use(VueAxios, Axios)

const api = Axios.create({
  baseURL: 'http://localhost:4000'
})

export default {
  login(creds, cb) {
    api.post('/auth/login', creds).then((res) => {
      cb(res.data);
    })
  },

  auth(data, cb) {
    api.post('/auth', data).then((res) => {
      cb(res.data);
    })
  }
}