const Role = require('../models/role')

const roleCheck = obj => {
  return new Promise((resolve, reject) => {
    let roleName = obj.role.toLowerCase()

    Role.find({name: roleName}, function(err, role) {
      if (err) throw err

      const $role = role[0];

      if (!$role.privileges.length) {
        reject('User have no privileges')
      } else if ($role.privileges.some(entity => obj.entity == entity) 
                  && $role[obj.action]) {
        resolve('Success')
      }
    })
  })
}

module.exports = roleCheck;