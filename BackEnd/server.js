const express     = require('express');
const app         = express();
const bodyParser  = require('body-parser');
const morgan      = require('morgan');
const mongoose    = require('mongoose');
const cors        = require('cors');

const config = require('./config'); // get our config file

const auth = require('./expressRoutes/auth');
const article = require('./expressRoutes/article');

const port = process.env.PORT || 4000; // used to create, sign, and verify tokens

// connect to database
mongoose.connect(config.database, config.auth).then(() => {
  console.info('Successfully connected to DB');
}); 

app.use(cors({origin: 'http://localhost:8080'}));

app.set('superSecret', config.secret); // secret variable

// use body parser so we can get info from POST and/or URL parameters
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// use morgan to log requests to the console
app.use(morgan('dev'));
app.use('/auth', auth);
app.use('/articles', article);

const server = require('http').createServer(app);
server.listen(port, function (error) {
	if (error) {
			console.error(error)
	} else {
			console.info("==> 🌎  Listening on port %s.", port)
	}
})