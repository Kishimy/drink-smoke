const express  = require('express');
const router   = express.Router();

const Article  = require('../models/article');
const jwtCheck = require('../helpers/jwtCheck');
const roleCheck = require('../helpers/role');

const entity = 'article';

router.route('/').get(function(req, res) {
  Article.find({}, function(err, articles) {
    if (err) throw err;

    res.json({
      success: true,
      articles: articles
    });
  })
});

router.route('/:id').get(function(req, res) {
  Article.find({_id: req.params.id}, function(err, article) {
    if (err) throw err;
  
    res.json({
      success: true,
      article: article[0]
    });
  });
});

router.use(jwtCheck);

router.route('/create').post(function(req, res) {
  roleCheck({
    role: req.decoded.role,
    entity: entity,
    action: 'create'
  })
  .then(
    () => {
      let article = new Article ({
        title: req.body.title, 
        content: req.body.content,
        date: Date.now(),
      });
    
      article.save(function(err) {
        if (err) throw err;
    
        console.log('article saved');
        res.json({ success: true });
      });
    },
    error => {
      console.log(error)
      res.json({ succcess: false })
    }
  );
});

module.exports = router;