const express    = require('express');
const auth       = express.Router();
const jwt        = require('jsonwebtoken');
const bcrypt     = require('bcrypt');

const User       = require('../models/user');
const jwtCheck   = require('../helpers/jwtCheck');

auth.route('/login').post(function(req, res) {
  User.findOne({
    name: req.body.username
  }, function(err, user) {
    if (err) throw err;

    if (!user) {
      res.json({
        success: false,
        message: 'Authentication failed. User not found'
      })
    } else if (user) {

      if (!bcrypt.compareSync(req.body.password, user.password)) {

        res.json({
          success: false,
          message: 'Authentication failed. Wrong password'
        })
      } else {

        const payload = {
          role: user.role
        };

        let token = jwt.sign(payload, req.app.get('superSecret'), {
          expiresIn: '1d'
        });

        res.json({
          success: true,
          message: 'Enjoy your token!',
          user: {
            name: user.name,
            role: user.role,
            token: token
          },
        });
      }
    }
  })
});

auth.use(function(req, res, next) {
  jwtCheck(req, res, next);
});

auth.route('/create').post(function(req, res) {
  if (req.body.password.length) {
    bcrypt.hash(req.body.password, 10, function(err, hash) {
      if (err) throw err

      let newUser = new User({
        name: req.body.username,
        password: hash,
        role: req.body.role
      })

      newUser.save(function(err) {
        if (err) throw err;

        console.log('User saved');
        res.json({ success: true });
      })
    })
  }
});

auth.route('/users').get(function(req, res) {
  User.find({}, function(err, users) {
    if (err) throw err;

    res.json(users);
  })
});

module.exports = auth;